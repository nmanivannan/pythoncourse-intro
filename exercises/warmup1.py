"""
Exercise warmup1
-----------------

Simple exercises to get used to python basics.

- From http://introtopython.org/var_string_num.html , do the following:
  - hello world
  - one variable, two messages
  - first name cases
  - full name
  - arithmetic

"""

#A='hello world'
print(A)
hello world
B="first exercise"
print(B)
first exercise
B="warmup exercises"
print(B)
warmup exercises
firstname='Nithyapriya'
print(firstname.lower())
nithyapriya
print(firstname.title())
Nithyapriya
print(firstname.upper())
NITHYAPRIYA

lastname='Manivannan'
print(firstname+lastname)
NithyapriyaManivannan
print(firstname+ ' '+lastname)
Nithyapriya Manivannan
A=56
B=32
print(A+B)
88
C=55.3
print(C//B)
1.0
print(C/B)
1.728125
print(A*B)
1792
print(A**C)
4.729311209479044e+96
print(A*C)
3096.7999999999997
F=(A+B)//C
print(F)
1.0
 Write your solution here
